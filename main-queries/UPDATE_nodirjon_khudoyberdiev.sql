--Alter the rental duration and rental rates of the film you inserted before to three weeks and 9.99, respectively.
UPDATE film
SET rental_duration = 21,  -- Three weeks
    rental_rate = 9.99
WHERE title = '22 Mile';

--Alter any existing customer in the database with at least 10 rental and 10 payment records. Change their personal data to yours (first name, last name, address, etc.). You can use any 
-- existing address from the "address" table. Please do not perform any updates on the "address" table, as this can impact multiple records with the same address.
UPDATE customer
SET first_name = 'Nodirjon',
    last_name = 'Khudoyberdiev',
    email = 'nodir@gmail.com',  -- Update with your email
    address_id = (SELECT address_id FROM address LIMIT 1)  -- Select any existing address
WHERE customer_id IN (
    SELECT customer_id
    FROM (
        SELECT c.customer_id
        FROM customer c
        JOIN rental r ON c.customer_id = r.customer_id
        JOIN payment p ON c.customer_id = p.customer_id
        GROUP BY c.customer_id
        HAVING COUNT(DISTINCT r.rental_id) >= 10 AND COUNT(DISTINCT p.payment_id) >= 10
    ) AS subquery
    LIMIT 1
);

-- Change the customer's create_date value to current_date.
UPDATE customer
SET create_date = current_date
WHERE customer_id IN (
    SELECT customer_id
    FROM (
        SELECT c.customer_id
        FROM customer c
        JOIN rental r ON c.customer_id = r.customer_id
        JOIN payment p ON c.customer_id = p.customer_id
        GROUP BY c.customer_id
        HAVING COUNT(DISTINCT r.rental_id) >= 10 AND COUNT(DISTINCT p.payment_id) >= 10
    ) AS subquery
    LIMIT 1
);